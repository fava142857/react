import React from 'react';
import ReactDOM from 'react-dom';
import FilterableProductTable from './component/FilterableProductTable.js'

const app = document.getElementById('app');
ReactDOM.render(<FilterableProductTable/>,app);